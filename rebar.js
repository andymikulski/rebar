/*!
 * Rebar 1.0
 * @author Andy Mikulski <gitlab.com/u/andymikulski>
 * @url https://gitlab.com/andymikulski/rebar
 */
;
(function(window, document, undefined) {

  var Rebar = (function(Rebar) {
    return {
      'defaultRegisters': [
        'div', 'p', 'span', 'a', 'input', 'form', 'ol', 'ul', 'li',
        'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
      ],

      'init': function() {
        var rebar = this;

        rebar._registerDefaults();

        return rebar;
      },

      '_registerDefaults': function() {
        var rebar = this,
          i;

        for (i = rebar.defaultRegisters.length - 1; i >= 0; i--) {
          rebar.register(rebar.defaultRegisters[i]);
        }

        return rebar;
      },

      'register': function(listToRegister) {
        var rebar = this;

        // convert the listToRegister into an array if it isn't already
        if (!(listToRegister instanceof Array)) {
          listToRegister = [listToRegister];
        }

        // for each item to register,
        for (var i = 0; i < listToRegister.length; i++) {
          // we just set up a new function
          // which ultimately just calls `printElement`
          rebar[listToRegister[i]] = (function(printedEl) {
            return function() {
              var args = Array.prototype.slice.call(arguments);
              return rebar.printElement(printedEl, (args.length &&
                  typeof args[0] === 'object') ? args.shift() : {},
                args.join(''));
            }
          }(listToRegister[i]));

          // if we only had one to register,
          // we can return that function
          // (so you can do:
          // var myDiv = rebar.register('myDiv');
          // myDiv('hi!');
          // )
          if (listToRegister.length === 1) {
            return rebar[listToRegister[i]];
          }
        }
      },

      'printElement': function(element, attributes, content) {
        var combinedString = '<' + element,
          attr;

        for (attr in attributes) {
          var safeAttr = attr.replace(/className/gi, 'class');
          combinedString += ' ' + safeAttr + '="' + attributes[attr] + '"';
        }

        combinedString += '>' + content + '</' + element + '>';

        return combinedString;
      }
    };
  })(window.Rebar || {});

  window.Rebar = Rebar.init();
  return window.Rebar;

})(window, document);