# Rebar
## HTML string generator + injector

Compose a set of elements using JS syntax, and get a composed string in return. Useful when you need to drop markup on the page you dont have control of.

## Usage

Include `rebar.js` or `rebar.min.js` on the page, or if you've got bower, `bower install andymikulski/rebar`.

Use rebar to construct 'elements' for you to use later:

```
const { div, span } = Rebar;
const newDiv = div('Text in the div!',
                span('Nested span!'),
                span({ className: 'my-class' }, 'Span with a class!')
              );
```

A compiled string is returned with which you can manipulate as you see fit.


## Usage Explained

```
  // pull out the references you need
  const { div, h1, form, span, input } = Rebar;

  // Define a container with a form, some labels, and various inputs
  let modalHTML =
    // If the first parameter is an object, those will be interpreted
    // as element attributes to embed during creation
    div({ className: 'modal' },
      h1('Log in to Rewriter CMS'),
      // nesting elements inside the form..
      form(
        span('Username'),
        input({
          type: 'text',
          name: 'username',
          required: 'true',
          name: 'txtUser'
        }),
        span('Password'),
        input({
          type: 'password',
          name: 'pass',
          required: 'true',
          name: 'txtPass'
        }),
        input({
          type: 'submit',
          value: 'Log in',
          name: 'btnLogin'
        }),
        input({
          type: 'submit',
          value: 'Cancel',
          name: 'btnCancel'
        })
      )
    );
```
Now `modalHTML` contains a string representing the HTML markup of all the stuff you just bunched together. Handling it from there is up to you.

For easy mode, you can use jQuery: `$(document.body).append(modalHTML);`


We don't have to write everything out by hand, either. We can use IIFE's and loops to return other Rebars, to make things a bit more dynamic.

```
  const { div, ul, li } = Rebar;
  // use a simple hash to match button caption -> button ref
  const itemControls = {
    'Save': 'btnSave',
    'Undo': 'btnUndo'
  };

  const controlPanelHTML =
    div(
      ul(
        // Use an IIFE to return a Rebar string
        (function() {
          let itemList = '';

          for (var fnCaption in itemControls) {
            // Since Rebar simply returns a string,
            // we can do simple concatenation to build lists
            itemList +=
              li({
                name: itemControls[fnCaption]
              }, fnCaption);
          }

          // return the string and Rebar keeps chugging along
          return itemList;
        })(),
        li('This is the last list element, and is hard-coded!')
      )
    );
```

## Registered elements

Rebar comes with a handful of common page elements built into its registry. Those elements are:

- div
- p
- span
- a
- input
- form
- ol
- ul
- li
- h1 - h6


If you'd like to use elements not in this list, simply use the `Rebar.register` function.

```
  var yourElement = Rebar.register('yourElement');

  var testWoo = yourElement('woo!');
  // testWoo === '<yourElement>woo!</yourElement>'
```

===


### Credits

Developed by [Andy Mikulski](http://www.andymikulski.com/).
